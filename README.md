# README
## APIs
### Login:
+ Request: .../login
	+ Method: POST
	+ Body: JSON
		+ username: String
		+ password: String
+ Response:
	+ Body: JSON (a string)
		+ "success": login successfully
		+ "failure": wrong information
### Create User:
+ Request: .../create
	+ Method: POST
	+ Body: JSON
		+ username: String
		+ password: String
+ Response:
	+ Body: JSON
		+ None empty content:
			+ id: String
			+ username: String
			+ password: String
		+ Empty content: wrong information
		