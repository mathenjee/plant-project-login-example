package com.mathenjee.example.logintest.controller;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.stereotype.Component;

import com.mathenjee.example.logintest.model.User;
import com.mathenjee.example.logintest.repository.UserRepository;

@Component
public class UserController {
	private UserRepository userRepository;
	private Log log;

	public UserController(UserRepository userRepository) {
		super();
		this.userRepository = userRepository;
		this.log = LogFactory.getLog(getClass());
	}

	public boolean login(User user) {
		if (user != null) {
			user = userRepository.findByUsernameAndPassword(user.getUsername(), user.getPassword());
			log.info("Login with " + user);
		}
		return user != null;
	}

	public User create(User user) {
		if (user != null) {
			user = userRepository.save(user);
			log.info("User \'" + user + "\' has been created.");
		}
		log.info("Do post(\'create\') with null-user.");
		return user;
	}
}
