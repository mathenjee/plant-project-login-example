package com.mathenjee.example.logintest.config;

import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;

import com.mongodb.MongoClient;
import com.mongodb.MongoClientURI;

@Configuration
public class MongoConfig extends AbstractMongoConfiguration {

	private static final String REMOTE_MONGO_URI = "mongodb://mathen:matkhau2297@notifyme-shard-00-00-xldpi.mongodb.net:27017,notifyme-shard-00-01-xldpi.mongodb.net:27017,notifyme-shard-00-02-xldpi.mongodb.net:27017/?ssl=true&replicaSet=NotifyMe-shard-0&authSource=admin";

	@Override
	public MongoClient mongoClient() {
		return new MongoClient(new MongoClientURI(REMOTE_MONGO_URI));
	}

	@Override
	protected String getDatabaseName() {
		return "login_test";
	}

}
