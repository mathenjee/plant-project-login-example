package com.mathenjee.example.logintest.repository;

import org.springframework.data.repository.CrudRepository;

import com.mathenjee.example.logintest.model.User;

public interface UserRepository extends CrudRepository<User, String> {
	User findByUsernameAndPassword(String username, String password);
}
