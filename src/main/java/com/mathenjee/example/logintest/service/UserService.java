package com.mathenjee.example.logintest.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.mathenjee.example.logintest.controller.UserController;
import com.mathenjee.example.logintest.model.User;

@RestController
public class UserService {
	@Autowired
	private UserController userController;

	public UserService(UserController loginController) {
		super();
		userController = loginController;
	}

	@PostMapping(value = "/login")
	public ResponseEntity<String> login(@RequestBody User user) {
		if (userController.login(user)) {
			return new ResponseEntity<String>("success", HttpStatus.OK);
		}

		return new ResponseEntity<>("failure", HttpStatus.OK);
	}

	@PostMapping(value = "/create")
	public ResponseEntity<User> create(@RequestBody User user) {
		user = userController.create(user);
		return new ResponseEntity<User>(user, HttpStatus.OK);
	}
}
